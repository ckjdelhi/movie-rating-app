import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MovieDetailService } from '../services/movie-detail.service';
import { MovieDetail } from '../model/MovieDetail';
@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {
  searchType = 's';
  searchKey = 'man';
  public movieDetails: MovieDetail[];
  constructor(public movieDetailService: MovieDetailService) {
    this.movieDetailService.get_movie_detail(this.searchKey, this.searchType).subscribe((res: MovieDetail[]) => {
      console.log(res);
      this.movieDetails = res;
  });
  }

  ngOnInit() {

  }

}
