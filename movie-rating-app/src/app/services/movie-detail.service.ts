import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MovieDetailService {

  constructor(private httpClient: HttpClient) { }

  public get_movie_detail(searchKey: string, searchType: string) {
    let url = 'http://www.omdbapi.com/?apikey=6aa0b481&{type}=';
    url = url.replace('{type}', searchType);
    return this.httpClient.get(url + searchKey);
  }
}
